<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Tweet extends Model implements HasMedia
{
    use HasFactory, HasMediaTrait;

    protected $fillable = ['title', 'user_id', 'body', 'img_id'];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('tweet')
            ->width(300)
            ->height(300)
            ->sharpen(10);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeWithLikes(Builder $query)
    {
        //TODO:: Deze query veranderen en zorgen dat het met laravel magie gemaakt word
        $query->leftJoinSub(
            'select tweet_id, sum(liked) likes, sum(!liked) dislikes from likes group by tweet_id',
            'likes',
            'likes.tweet_id',
            'tweets.id'
        );
    }

    public function isLikedBy(User $user)
    {
        return (bool)$user->likes
            ->where('tweet_id', $this->id)
            ->where('liked', true)
            ->count();
    }

    public function isDislikedBy(User $user)
    {
        return (bool)$user->likes
            ->where('tweet_id', $this->id)
            ->where('liked', false)
            ->count();
    }

    public function dislike($user = null)
    {
        return $this->like($user, false);
    }

    public function like($user = null, $liked = true): void
    {
        $this->likes()->updateOrCreate(
            [
                'user_id' => $user->id ?? auth()->id(),
            ],
            [
                'liked' => $liked,
            ]
        );
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
