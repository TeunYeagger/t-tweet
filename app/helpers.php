<?php

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

function current_user()
{
    return auth()->user();
}

function makeRole($name)
{
    $role = Role::firstOrCreate(['name' => $name]);
    return $role;
}

function destroyRole($id)
{
    $role = Role::destroy($id);
}

function findRole($name)
{
    $role = Role::findByName($name);
    return $role;
}

function makePermission($name)
{
    $role = Permission::firstOrCreate(['name' => $name]);
    return $role;
}

function destroyPermission($id)
{
    $permission = Permission::destroy($id);
}

function findPermission($name)
{
    $permission = Permission::findByName($name);
    return $permission;
}

function userGetRole($role)
{
    auth()->user()->assignRole($role);
//            $users = User::role($role)->get();
}

function userDropRole($role)
{
    auth()->user()->removeRole($role);
}

function getAllPermissionUser()
{
    $permissions = auth()->user()->getAllPermissions();
    return $permissions;
}

function getAllRolesUser()
{
    $roles = auth()->user()->roles();
    return $roles;
}

function getAllUsersWithRole($role)
{
    $users = User::role($role)->get();
    return $users;
}

function switchRole()
{
    if (auth()->user()->hasRole('user')) {
        makeRole('admin');
        userDropRole('user');
        userGetRole('admin');
    } elseif (auth()->user()->hasRole('admin')) {
        makeRole('user');
        userDropRole('admin');
        userGetRole('user');
    } else {
        return 'Je hebt helemaal geen Roll';
    }
}
