<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class PermissionsController extends Controller
{
    public function switchMyPermission(){

        echo 'getAllPermissionUser:';
        echo "<br/>";
        echo getAllPermissionUser();
        echo "<br/><br/>";
        switchRole();
        echo 'getAllPermissionUserNow:';
        echo "<br/>";
        echo getAllPermissionUser();
        echo "<br/><br/>";

        return view('test-permissions');
    }
}
