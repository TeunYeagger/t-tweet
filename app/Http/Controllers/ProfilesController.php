<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tweet;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class ProfilesController extends Controller
{
    use HasRoles;

    public function show(User $user)
    {
        return view('profiles.show', [
            'user' => $user,
            'tweets' => $user->tweets()->withLikes()->paginate(300),
        ]);
    }

    public function edit(User $user)
    {
        return view('profiles.edit', compact('user'));
    }

    public function update(User $user)
    {
        $attributes = request()->validate([
            'username' => ['string', 'required', 'max:30', 'alpha_dash', Rule::unique('users')->ignore($user),],
            'name' => ['string', 'required', 'max:30'],
            'description' => ['string', 'max:255'],
            'avatar' => ['image'],
            'banner' => ['image'],
            'email' => ['string', 'required', 'email', 'max:50', Rule::unique('users')->ignore($user),],
            'password' => ['string', 'required', 'min:8', 'max:60', 'confirmed',],
        ]);

        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars', 'public');
        }
        if (request('banner')) {
            $attributes['banner'] = request('banner')->store('banner', 'public');
        }

        $user->update($attributes);

        return redirect($user->path());
    }
}
