<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tweet;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index', ['users' => User::paginate(20), 'tweets' => Tweet::paginate(10)]);
    }

    public function showTweets()
    {
        return view('admin.tweets', ['tweets' => Tweet::paginate(10)]);
    }

    public function showUsers()
    {
        return view('admin.users', ['users' => User::paginate(20)]);
    }

    public function adminTweetEdit(Tweet $tweet)
    {
        return view('admin.edit', compact('tweet'));
    }

    public function adminTweetUpdate(Tweet $tweet)
    {
        $attributes = request()->validate([
            'body' => ['string', 'max:255'],
        ]);

        $tweet->update($attributes);
        return redirect(route('admin.index'));
    }

    public function adminTweetDestroy(Tweet $tweet)
    {
        $deuser = Tweet::find($tweet)->first();
        if (!$deuser) {
            abort(404);
        } else {
            $deuser->delete();
        }

        return back();
    }

    public function adminUserEdit(User $user)
    {
        return view('admin.user-edit', compact('user'));
    }

    public function adminUserUpdate(User $user)
    {
        $attributes = request()->validate([
            'username' => ['string', 'required', 'max:255', 'alpha_dash', Rule::unique('users')->ignore($user),],
            'name' => ['string', 'required', 'max:255'],
            'description' => ['string', 'max:255'],
            'avatar' => ['image'],
            'banner' => ['image'],
            'email' => ['string', 'required', 'email', 'max:255', Rule::unique('users')->ignore($user),],
        ]);
        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars', 'public');
        }
        if (request('banner')) {
            $attributes['banner'] = request('banner')->store('banner', 'public');
        }

        $user->update($attributes);

        return redirect(route('admin.users'));
    }

    public function adminUserUpdateAdmin(User $user){
        if ($user->hasRole('user')) {
            $user->removeRole('user');
            $user->assignRole('admin');
            return back();
        }else {
            return redirect(route('admin.users'));
        }
    }

    public function adminUserUpdateUser(User $user){
        if ($user->hasRole('admin')) {
            $user->removeRole('admin');
            $user->assignRole('user');
            return back();
        }else{
            return redirect(route('admin.users'));
        }
    }
}
