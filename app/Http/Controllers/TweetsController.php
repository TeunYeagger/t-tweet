<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class TweetsController extends Controller
{
    public function store(Request $request)
    {

        $attributes = request()->validate([
            'body' => ['required','max:255','string'],
            'image_url' => ['image', 'max:20000'],
        ]);
        $file = request('image_url');

        $tweet = Tweet::create([
            'user_id' => auth()->id(),
            'body' => $attributes['body'],
        ]);
        if($file){
            $tweet->addMedia($file)->toMediaCollection('tweet_img');
        }

        return redirect()->route('home');
    }
    public function index()
    {

        if(!auth()->user()->hasAnyRole('admin', 'user')){
            makeRole('user');
            userGetRole('user');
        }

        return view('tweets.index', ['tweets' => auth()->user()->timeline()]);
    }
}
