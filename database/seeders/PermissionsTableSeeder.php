<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permission_tree = [
            [
                'name' => 'user', // Role name
                'guard_name' => 'web',
                'permissions' => [

                    // Tweets
                    ['name' => 'tweet_create_any'],
                    ['name' => 'tweet_see_any'],
                    ['name' => 'tweet_destroy'],

                    // Profile
                    ['name' => 'user_edit']
                ],
            ],
            [
                'name' => 'admin',
                'guard_name' => 'web',
                'permissions' => [
                    // Tweets
                    ['name' => 'tweet_create_any'],
                    ['name' => 'tweet_see_any'],
                    ['name' => 'tweet_edit_any'],
                    ['name' => 'tweet_destroy_any'],
                    ['name' => 'tweet_destroy'],

                    // User
                    ['name' => 'user_edit_any'],
                    ['name' => 'user_edit'],

                    // Admin
                    ['name' => 'see_admin_page'],
                ],
            ],
        ];

        foreach ($permission_tree as $role) {

            $spatie_role = Role::firstOrCreate([
                'name' => $role['name'],
                'guard_name' => $role['guard_name'],
            ]);

            foreach ($role['permissions'] as $permission) {
                $spatie_permission = Permission::firstOrCreate([
                    'name' => $permission['name'],
                    'guard_name' => $role['guard_name'],
                ]);

                $spatie_role->givePermissionTo($spatie_permission);
            }

        }

    }
}
