<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 1) as $index) {
            DB::table('admins')->insert([
                'username' => 'Admin',
                'name' => 'Admin',
                'email' => 'admin@teun.nl',
                'email_verified_at' => now(),
                'description' => $faker->paragraph(2),
                'password' => Hash::make('123456789'),
                'remember_token' => Str::random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
