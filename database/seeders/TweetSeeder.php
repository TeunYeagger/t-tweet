<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class TweetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $dateTime = now();
        foreach (range(1, 20) as $index) {
            DB::table('tweets')->insert([
                // 'title' => $faker->sentence(3),
                'user_id' => rand(1, 10),
                // 'body' => $faker->paragraph(3),
                'body' => 'Dit is de body lormLorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio at, deserunt quasi aut adipisci corrupti obcaecati reprehenderit ratione et. Sapiente qui aut dolore iure, enim vel reprehenderit perferendis eligendi itaque.',
                'created_at' => $dateTime,
                'updated_at' => $dateTime,
            ]);
        }
    }
}
