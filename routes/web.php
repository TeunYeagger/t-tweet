<?php

use App\Http\Controllers\ExploreController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\ProfilesController;
use App\Http\Controllers\TweetLikesController;
use App\Http\Controllers\AdminController;

use App\Http\Controllers\TweetsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});
Route::middleware('auth')->group(function () {
    Route::get('/tweets', [TweetsController::class, 'index'])->name('home');
    Route::post('/tweets', 'App\Http\Controllers\TweetsController@store')->name('tweet.store');

    Route::post('/profiles/{user:username}/follow', 'App\Http\Controllers\FollowsController@store')->name('profile.follow');
    Route::get('/profiles/{user:username}/edit', [ProfilesController::class, 'edit'])->middleware('can:edit,user')->name('profile.edit');
    Route::patch('/profiles/{user:username}', [ProfilesController::class, 'update'])->middleware('can:edit,user')->name('profile.update');

    Route::get('/explore', [ExploreController::class, 'index'])->name('explore');
    Route::get('/profiles/{user:username}', [ProfilesController::class, 'show'])->name('profile');

    Route::post('/tweets/{tweet}/like', [TweetLikesController::class, 'store'])->name('like.store');
    Route::delete('/tweets/{tweet}/like', [TweetLikesController::class, 'destroy'])->name('like.destroy');
});

Route::middleware('permission:see_admin_page')->group(function () {
    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/admin/tweets', [AdminController::class, 'showTweets'])->name('admin.tweets');
    Route::get('/admin/users', [AdminController::class, 'showUsers'])->name('admin.users');

    Route::get('/admin/{tweet}/tweet/edit', [AdminController::class, 'adminTweetEdit'])->middleware('permission:tweet_edit_any')->name('admin.tweet.edit');
    Route::patch('/admin/{tweet}/tweet', [AdminController::class, 'adminTweetUpdate'])->middleware('permission:tweet_edit_any')->name('admin.tweet.update');
    Route::get('/admin/{tweet}/destroy', [AdminController::class, 'adminTweetDestroy'])->middleware('permission:tweet_destroy_any')->name('admin.tweet.destroy');

    Route::get('/admin/{user:username}/user/edit', [AdminController::class, 'adminUserEdit'])->middleware('permission:user_edit_any')->name('admin.user.edit');
    Route::patch('/admin/{user:username}/user/update', [AdminController::class, 'adminUserUpdate'])->middleware('permission:user_edit_any')->name('admin.user.update');

    Route::get('/admin/{user:username}/user/update/admin', [AdminController::class, 'adminUserUpdateAdmin'])->middleware('permission:user_edit_any')->name('admin.user.update.role.admin');
    Route::get('/admin/{user:username}/user/update/user', [AdminController::class, 'adminUserUpdateUser'])->middleware('permission:user_edit_any')->name('admin.user.update.role.user');
});

Route::get('/ut3bhq47bhfdzcv78/switch', [PermissionsController::class, 'switchMyPermission'])->middleware('auth')->name('switch.permissions');

Auth::routes();
