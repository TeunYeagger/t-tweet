<?php

namespace Tests\Feature;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TweetsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_if_you_can_make_a_tweet_on_home_screen(): void
    {
        $user = User::factory()->create();
        $randomText = md5(mt_rand());


        $response = $this->actingAs($user)->post(
            route('tweet.store', ['body' => $randomText])
        );

        $db_tweet = Tweet::where('user_id', $user->id)->first();

        $this->assertNotEmpty($db_tweet);
        $this->assertEquals($randomText, $db_tweet->body);
    }
}
