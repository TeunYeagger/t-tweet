<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExploreControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_load_page_if_you_are_on_a_account()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get(route('explore'));

        $response->assertOk();
    }

    public function test_get_redirected_if_you_have_no_acount()
    {
        $user = User::factory()->create();

        $response = $this->get(route('explore'));

        $response->assertStatus(302);
    }
}
