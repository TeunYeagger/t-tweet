<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditUserProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_you_can_update_your_profiel_information()
    {
        $user = User::factory()->create(['password' => 'password']);

        $this->actingAs($user);

        $nieuweUsername = "JimmyJones69";
        $nieuweName = "Jimmy Jones";
        $nieuweDescription = "This is the nieuw description of Jimmy.";
        $nieuweEmail = "Jimmy@Jones.nl";

        $allDataToUpdate = [
            'user' => $user,
            'username' => $nieuweUsername,
            'name' => $nieuweName,
            'description' => $nieuweDescription,
            'email' => $nieuweEmail,
            'password' => 'password',
            'password_confirmation' => 'password',
            ];

        $this->patch(route('profile.update', $allDataToUpdate));

        $db_user = User::find($user->id)->first();

        $this->assertSame($nieuweUsername, $db_user->username);
        $this->assertSame($nieuweName, $db_user->name);
        $this->assertSame($nieuweDescription, $db_user->description);
        $this->assertSame($nieuweEmail, $db_user->email);
    }
}
