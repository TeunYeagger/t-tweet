<?php

namespace Tests\Feature;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LikeAPostTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_like_post(): void
    {
        $user = User::factory()->create();
        $tweet = Tweet::factory()->create();

        $this->actingAs($user);
        $this->post(route('like.store', $tweet->id));

        $this->assertTrue($tweet->isLikedBy($user));
    }
}
