<?php

namespace Tests\Feature;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetToAllPagesTest extends TestCase
{
    use RefreshDatabase;
//    use DatabaseMigrations;

    public function test_can_get_to_start_page()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function test_can_get_to_login_page()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    public function test_can_get_to_register_page()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
    }

    public function test_can_get_to_home_page()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get(route('home'));
        $response->assertStatus(200);
    }

    public function test_can_get_to_profile_page()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->get(route('profile', $user->username));
        $response->assertStatus(200);
    }

    public function test_can_get_to_explore_page()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get(route('explore'));
        $response->assertStatus(200);
    }

    public function test_can_get_to_profile_edit_page()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get(route('profile.edit', $user->username));
        $response->assertStatus(200);
    }
}
