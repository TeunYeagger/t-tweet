<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserAddAndDeleteFriendsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_add_a_friend()
    {
        $user = User::factory()->create();
        $friendProfile = User::factory()->create();

        $this->actingAs($user);

        $response = $this->post(route('profile.follow', $friendProfile->username));

        $isFollowing = $user->following($friendProfile);

        $this->assertTrue($isFollowing);
    }

    public function test_user_can_delete_a_friend()
    {
        $user = User::factory()->create();
        $friendProfile = User::factory()->create();

        $this->actingAs($user);

        $this->post(route('profile.follow', $friendProfile->username));
        $isFollowing = $user->following($friendProfile);
        $this->assertTrue($isFollowing);

        $this->post(route('profile.follow', $friendProfile->username));
        $isNotFollowing = $user->following($friendProfile);
        $this->assertFalse($isNotFollowing);

    }
}
