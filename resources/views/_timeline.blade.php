<div class="border border-gray-300 rounded-lg bg-white">
    @forelse ($tweets as $tweet)
    @include('_tweet')
    @empty
        <p class="p4-">No tweets yet.</p>
    @endforelse
    {{$tweets->links()}}
</div>
