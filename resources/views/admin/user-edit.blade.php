<x-app>
    <form method="POST" action="{{ route('admin.user.update', $user->username) }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="mb-5">
            @if($user->hasRole('user'))
                <p>Is User</p>
            <a class="font-bold hover:text-blue-500 pr-5"
               href="{{ route('admin.user.update.role.admin', $user->username) }}">Make Admin</a>
            @elseif($user->hasRole('admin'))
                    <p>Is Admin</p>
                <a class="font-bold hover:text-blue-500" href="{{ route('admin.user.update.role.user', $user->username) }}">Make User</a>
            @else
                <p>Nog geen rol</p>
            @endif


        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                   for="name"
            >
                Name
            </label>

            <input class="border border-gray-400 p-2 w-full"
                   type="text"
                   name="name"
                   id="name"
                   value="{{ $user->name }}"
                   required
            >

            @error('name')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                   for="username"
            >
                Username
            </label>

            <input class="border border-gray-400 p-2 w-full"
                   type="text"
                   name="username"
                   id="username"
                   value="{{ $user->username }}"
                   required
            >

            @error('username')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                   for="description"
            >
                description
            </label>

            <input class="border border-gray-400 p-2 w-full"
                   type="text"
                   name="description"
                   id="description"
                   value="{{ $user->description }}"
                   required
            >

            @error('description')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                   for="avatar"
            >
                Avatar
            </label>

            <div class="flex">
                <input class="border border-gray-400 p-2 w-full"
                       type="file"
                       name="avatar"
                       id="avatar">
                {{--                @dd($user->avatar)--}}
                <img src="{{ $user->getAvatarUrl()}}" class="w-20">
            </div>

            @error('avatar')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>


        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                   for="banner"
            >
                Banner
            </label>

            <div class="flex">
                <input class="border border-gray-400 p-2 w-full"
                       type="file"
                       name="banner"
                       id="banner">
                <img src="{{ $user->getBannerUrl()}}" class="w-20">
            </div>

            @error('avatar')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>


        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                   for="email"
            >
                Email
            </label>

            <input class="border border-gray-400 p-2 w-full"
                   type="email"
                   name="email"
                   id="email"
                   value="{{ $user->email }}"
                   required
            >

            @error('email')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <button type="submit"
                    class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500"
            >
                Submit
            </button>
            <a class="hover:underline" href="{{route('admin.users')}}">Cancel</a>
        </div>
    </form>
</x-app>
