<x-app>
    <x-user-tweet-button/>
    <div class="grid grid-cols-1 md:grid-cols-2 gap-1">
        <div class="justify-self-center">
            @foreach($users as $user)
                <div class="pb-1">
                <div class="justify-self-center w-full bg-white border border-gray-400 rounded">
                    <a href="{{$user->path()}}">
                        <div class="flex items-center justify-self-center">
                            <img class="mr-4 rounded w-10 h-10" src="{{$user->getAvatarUrl()}}"
                                 alt="{{$user->username}}'s avatar"
                                 width="60">
                            <p class="place-self-center @if($user->hasRole('admin')) font-italic @else font-bold @endif">{{ '@' . $user->username}}</p>
                        </div>
                    </a>
                </div>
                </div>
            @endforeach
        </div>
        <div class="justify-self-center pb-5">
            <div class="border border-gray-400 rounded-lg bg-white">
                @forelse ($tweets as $tweet)
                    <div class="flex p-4 {{$loop->last ? '' : 'border-b border-gray-300'}} overflow-hidden h-32">
                        <div class="mr-2 flex-shrink-0">
                            <a href="{{route('profile', $tweet->user)}}">
                                <img class="rounded-full mr-2 w-16 h-16" src="{{$tweet->user->getAvatarUrl()}}" alt="">
                            </a>
                        </div>
                        <div class="">
                            <a href="{{route('profile', $tweet->user)}}">
                                <h5 class="font-bold text-sm mb-1">{{$tweet->user->name}}</h5>
                            </a>
                            <p class="text-xs">
                                {{$tweet->body}}
                            </p>
                        </div>
                    </div>
                @empty
                    <p class="p4-">No tweets yet.</p>
                @endforelse
            </div>
        </div>
    </div>
</x-app>
