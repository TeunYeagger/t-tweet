<x-app>
    <form method="POST" action="{{ route('admin.tweet.update', $tweet->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="body">
                Body
            </label>
            <textarea class="border border-gray-400 p-2 w-full" rows="10" type="text" name="body" id="body" required>{{ $tweet->body }}</textarea>

            @error('body')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>


        <div class="mb-6">
            <button type="submit"
                    class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500">
                Submit
            </button>
            <a class="hover:underline" href="{{route('admin.tweets')}}">Cancel</a>
        </div>
    </form>
</x-app>

