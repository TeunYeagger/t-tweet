<x-app>
    <x-user-tweet-button/>
    <div class="justify-self-center">
        {{$users->links()}}
        <div class="grid grid-cols-1 lg:grid-cols-2 overflow-hidden">
            @foreach($users as $user)
                <div class="pb-1 pr-1">
                <div class="grid grid-cols-2 justify-self-center w-full h-full bg-white border border-gray-400 rounded">
                    <a href="{{$user->path()}}">
                        <div class="flex items-center justify-self-center">
                            <img class="mr-4 rounded w-20 h-20" src="{{$user->getAvatarUrl()}}"
                                 alt="{{$user->username}}'s avatar"
                                 width="60">
                            <p class="place-self-center text-md sm:text-lg @if($user->hasRole('admin')) font-italic @else font-bold @endif">{{ '@' . $user->username}}</p>

                        </div>
                    </a>

                    <div class="place-self-center text-sm sm:text-md font-bold grid grid-cols-1">
                        <a href="{{route('admin.user.edit', $user->username)}}" class="hover:text-blue-700">Edit</a>
                    </div>
                </div>
                </div>
            @endforeach
        </div>
    </div>
</x-app>
