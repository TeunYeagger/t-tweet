<x-app>
    <x-user-tweet-button/>
    <div class="justify-self-center pb-5">
        {{$tweets->links()}}
        <div class="border border-gray-400 rounded-lg bg-white">
            @forelse ($tweets as $tweet)
                <div class="flex p-4 {{$loop->last ? '' : 'border-b border-gray-300'}} overflow-hidden h-32">
                    <div class="mr-2 flex-shrink-0">
                        <a href="{{route('profile', $tweet->user)}}">
                            <img class="rounded-full mr-2 w-16 h-16" src="{{$tweet->user->getAvatarUrl()}}" alt="">
                        </a>
                    </div>
                    <div class="w-full">
                        <a href="{{route('profile', $tweet->user)}}">
                            <h5 class="text-sm mb-1 @if($tweet->user->hasRole('admin')) font-italic @else font-bold @endif">{{$tweet->user->name}}</h5>
                        </a>
                        <p class="text-xs">
                            {{$tweet->body}}
                        </p>
                    </div>
                    @if($tweet->getFirstMediaUrl('tweet_img'))
                        <div class="flex object-cover max-w-xs max-h-52 items-end overflow-hidden object-cover">
                            <img class="max-w-20 m-auto" src="{{ $tweet->getFirstMediaUrl('tweet_img', 'tweet') }}" alt="img">
                        </div>
                    @endif
                    <div class="grid grid-cols-1 w-32 m-3">
                        <a class="font-bold text-sm p-1 hover:text-blue-700" href="{{route('admin.tweet.edit',$tweet->id)}}">Edit</a>
                        <a class="font-bold text-sm p-1 hover:text-red-500" href="{{route('admin.tweet.destroy',$tweet->id)}}">Delete</a>
                    </div>
                </div>
            @empty
                <p class="p4-">No tweets yet.</p>
            @endforelse
        </div>
    </div>
</x-app>
