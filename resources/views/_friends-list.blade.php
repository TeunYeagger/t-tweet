<div class="lg:w-48 bg-white border border-gray-400 rounded-lg p-4 lg:ml-auto mt-5 lg:mt-0">
<h3 class="font-bold m-1 text-xl md-4">Friends</h3>
<ul>
    @forelse (auth()->user()->follows as $user)
        <li class="{{ $loop->last ? '' : 'mb-4' }} place-self-center">
            <div>
                <a href="{{route('profile', $user->username)}}" class="flex items-center text-sm a">
                <img class="rounded-full mx-2 w-10 h-10" src="{{$user->getAvatarUrl()}}" alt="No img">
                {{$user->name}}
                </a>
            </div>
        </li>
    @empty
        <p class="p-4">No friends yet.</p>
    @endforelse
</ul>
</div>
