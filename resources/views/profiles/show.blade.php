<x-app>
    <header class="mb-6 relative bg-white border border-gray-400 rounded-xl -mt-1 -mx-0.5">
        <div class="relative">
            <img src="{{$user->getBannerUrl()}}" alt="banner" class="rounded-lg mb-2 w-full h-64 object-cover">
            <img class="rounded-full mr-2 invisible sm:visible w-50 h-50 object-cover absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" width="150" height="150" style="width: 150px; left:50%;" src="{{$user->getAvatarUrl()}}" alt="">
        </div>
        <div class="flex justify-between items-center">
            <div class='mb-6 ml-1' style="max-width: 300px">
                <h2 class="font-bold text-lg lg:text-xl">{{$user->name}}</h2>
                <p class="text-xs lg:text-sm">Joined {{$user->created_at->diffForHumans()}}</p>
            </div>

            <div class="flex">
                @can('user_edit_any')
                    <a href="{{route('admin.user.edit', $user->username)}}" class="text-black rounded-full border border-gray-500 mr-1 py-2 px-2 text-xs">Admin Edit</a>
                @endcan
                @can('edit', $user)
                    <a href="{{route('profile.edit', $user)}}" class="text-black rounded-full border border-gray-500 mb-auto mr-1 py-2 px-2 text-xs">
                        Edit Profile
                    </a>
                @endcan

                    <x-follow-button :user="$user"></x-follow-button>

            </div>
        </div>
        <p class="text-md ml-1 overflow-hidden">{{$user->description}}</p>
    </header>
    @include('_timeline', ['tweets' => $tweets])
</x-app>
