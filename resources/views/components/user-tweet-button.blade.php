<div class="grid grid-cols-1 md:grid-cols-3 gap-1 pb-5">
    <div class="justify-self-center">
        <a href="{{route('admin.users')}}" class="w-100 justify-self-center font-bold hover:text-blue-500 text-lg m-2">Users</a>
    </div>
    <div class="justify-self-center">
        <a href="{{route('admin.index')}}" class="w-100 justify-self-center font-bold hover:text-blue-500 text-lg m-2">Index</a>
    </div>
    <div class="justify-self-center">
        <a href="{{route('admin.tweets')}}" class="w-100 justify-self-center font-bold hover:text-blue-500 text-lg m-2">Tweets</a>
    </div>
</div>
