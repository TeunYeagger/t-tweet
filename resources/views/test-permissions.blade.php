@role('admin')
<h2>I am a Admin!</h2>
@endrole
@role('user')
<h2>I am a User!</h2>
@endrole



@can('all')
    <p>Je mag alles.</p>
@endcan
@can('write_tweet')
    <p>Je mag tweets maken.</p>
@endcan
@can('see_tweet')
    <p>Je mag tweets bekijken.</p>
@endcan
