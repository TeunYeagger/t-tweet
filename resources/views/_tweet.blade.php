<div class="flex p-4 {{$loop->last ? '' : 'border-b border-gray-300'}} overflow-hidden">
    <div class="mr-2 flex-shrink-0 self-center">
        <a href="{{route('profile', $tweet->user)}}">
       <img class="rounded-full mr-2 sm:w-20 w-14" src="{{$tweet->user->getAvatarUrl()}}" alt="">
    </a>
    </div>
    <div class="w-full self-center">
        <a href="{{route('profile', $tweet->user)}}">
        <h5 class="font-bold mb-4">{{$tweet->user->name}}</h5>
        </a>
        <div class="overflow-hidden w-full">
        <p class="text-sm">
           {{$tweet->body}}
       </p>
        </div>
        @auth
            <x-like-buttons :tweet="$tweet" />
        @endauth

    </div>

    @if($tweet->getFirstMediaUrl('tweet_img'))
        <div class="flex object-cover max-w-xs max-h-52 items-end overflow-hidden object-cover">
            <img class="max-w-20 m-auto" src="{{ $tweet->getFirstMediaUrl('tweet_img', 'tweet') }}" alt="img">
        </div>
    @endif
</div>
