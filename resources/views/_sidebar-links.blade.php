<ul class="grid grid-cols-4 lg:grid lg:grid-cols-1">
    <li class="pb-4">
        <a href="{{ route('home') }}" class="font-bold text-m sm:text-lg mb-4 black hover:text-blue-300 transition">
        Home
        </a>
    </li>
    <li class="pb-4">
        <a href="{{route('explore')}}" class="font-bold text-m sm:text-lg mb-4 black hover:text-blue-300 transition">
        Exploare
        </a>
    </li>
    <li class="pb-4">
        <a href="{{route('profile', auth()->user())}}" class="font-bold text-m sm:text-lg mb-4 black hover:text-blue-300 transition">
        Profile
        </a>
    </li>
    <li class="pb-4">
        <a class="font-bold mb-4 black hover:text-blue-300 text-m sm:text-lg transition" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
         {{ __('Logout') }}
     </a>
     <form id="logout-form" action="{{ route('logout') }}" method="POST" class="">
         @csrf
     </form>
    </li>
    @can('see_admin_page')
    <li class="pb-4">
        <a href="{{route('admin.index')}}" class="font-bold text-m sm:text-lg mb-4 black hover:text-blue-300 transition">
            Admin
        </a>
    </li>
    @endcan
</ul>

