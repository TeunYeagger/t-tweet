<x-app>
    <div class="grid grid-cols-1 sm:grid-cols-2 gap-4">
        @foreach($users as $user)
            <a class="" href="{{$user->path()}}">
            <div class="justify-self-center w-full h-full bg-white border border-gray-400 rounded">

                    <div class="flex items-center justify-self-center">
                        <img class="mr-4 rounded w-20 h-20" src="{{$user->getAvatarUrl()}}" alt="{{$user->username}}'s avatar"
                             width="60">
                        <p class="place-self-center font-bold">{{ '@' . $user->username}}</p>
                    </div>

            </div>
            </a>
        @endforeach
    </div>
    {{$users->links()}}

</x-app>
