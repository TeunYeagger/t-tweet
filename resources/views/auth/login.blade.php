<x-master>
    <div class="container mx-auto flex justify-center ">
        <div class="px-6 py-4 bg-blue-200 rounded">
            <div class="col-md-8">
                <div class="card-header font-bold text-lg mb-4"> {{ isset($url) ? ucwords($url) : ""}} {{ __('Login') }}</div>
                @isset($url)
                    <div method="POST" action='{{ url("login/$url") }}'>
                        @else
                            <form method="POST" action='{{ route('login') }}'>
                                @endisset

                                @csrf
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}" required autocomplete="email"
                                               autofocus>

                                        @error('email')
                                        <br/>
                                        <strong>{{ $message }}</strong>
                                        @enderror
                                    </div>
                                </div>


                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password" required autocomplete="current-password">

                                    @error('password')
                                    <br/>
                                    <strong>{{ $message }}</strong>
                                    @enderror
                                </div>


                                <div class="">
                                    <div class="">
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>


                                <div class="col-md-8 offset-md-4">
                                    <button type="submit"
                                            class="px-3 py-2 rounded text-sm uppercase bg-blue-600 text-white mt-3">
                                        Submit
                                    </button>
                                    <br>
                                    <a class="text-sm hover:text-blue-500 m-1"
                                       href="{{ route('password.request') }}">Forgot Your Password?</a>
                                    <a class="text-sm hover:text-blue-500"
                                       href="{{ route('register') }}">Register?</a>
                                </div>


                            </form>
                    </div>
            </div>
        </div>
    </div>
</x-master>
