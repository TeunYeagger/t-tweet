<div class="border border-gray-400 rounded-lg py-6 px-8 mb-8 bg-white">
    <form method="POST" action="{{route('tweet.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="flex">
        <textarea name="body" class="w-full border border-gray-300 rounded p-2" placeholder="What's up doc?" required></textarea>
        @error("body")
        <p class="text-red-500">{{$message}}</p>
        @enderror
        @error("user_id")
        <p class="text-red-500">{{$message}}</p>
        @enderror

            <label class="w-32 flex flex-col items-center px-1 py-1 mx-2 bg-white text-gray-400 rounded-lg shadow-lg transition  tracking-wide uppercase border border-gray-300 cursor-pointer hover:border-blue-400 hover:bg-blue-400 hover:text-white">
                <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                </svg>
                <span class="mt-1 text-xs">Select a image</span>
                <input type='file' class="hidden" name="image_url" id="image_url"/>
            </label>
        @error("image_url")
        <p class="text-red-500">{{$message}}</p>
        @enderror
        </div>
        <footer class="flex justify-between items-center mt-5">
             <img class="rounded-full mr-1 h-10" src="{{auth()->user()->getAvatarUrl()}}" alt="Avatar">
             <button type="submit" class="text-white bg-blue-400 rounded-lg shadow py-2 px-2 h-10 hover:bg-blue-600 transition ">
                 Tweet-me-man
             </button>
         </footer>
     </form>
</div>
