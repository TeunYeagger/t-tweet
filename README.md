## T-Tweet

Dit is een twitter clone waar op je tweets kan plaatsen.
### Todo:
Hier staan wat punten die nog kunenn gedaan worden aan de website.
<ul>
    <li>
        De profile banner image kunnen veranderen.[Done]
    </li>
    <li>
        De profile description kunnen veranderen.[Done]
    </li>
    <li>
        Admin Page.[Done]
    </li>
    <li>
        Tweets kunnen delete.
    </li>
    <li>
        Unlike a tweet.
    </li>
    <li>
        Foto's bij je tweets kunnen zetten.[Done]
    </li>
    <li>
        Responsive.[Ok]
    </li>
    <li>
        Meta data van de foto's stripen bij de tweets.[Done]
    </li>
</ul>
